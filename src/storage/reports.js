const { get } = require('./db');
const SQL = require('sql-template-strings');

exports.films = async ({ from, to }) => {
  const query = SQL`SELECT c.name,
                           count(distinct fa.id) as filmsCnt,
                           count(distinct o.id)  as ordersCnt,
                           sum(o.price)          as money
                    FROM category c
                             LEFT JOIN film f on f.categoryId = c.id
                             LEFT JOIN film fa on fa.categoryId = c.id AND fa.isActive = 1
                             LEFT JOIN 'order' o ON o.filmId = f.id AND (o.createDate BETWEEN ${from} AND ${to})
                    GROUP BY c.id`;

  return await get().all(query);
};

exports.money = async ({ from, to }) => {
  const query = SQL`
      SELECT A.*,
             IFNULL(B.fees, 0) as fees
      FROM (SELECT COUNT(o.id),
                   SUM(o.price)                                            as money,
                   SUM((o.price / (100.0 - o.discount)) * 100.0 - o.price) as discounts,
                   date(o.createDate, 'unixepoch')                         as day
            FROM 'order' o
            WHERE (o.createDate BETWEEN 1580641349 AND 2781246148)
            GROUP by day) A
               LEFT JOIN (
          SELECT date(o.closeDate, 'unixepoch')         as day,
                 SUM(o.bondTaken) - SUM(o.bondReturned) as fees
          FROM 'order' o
          WHERE (o.closeDate BETWEEN ${from} AND ${to})
          GROUP by day
      ) B on B.day = A.day  
  `;

  return await get().all(query);
};