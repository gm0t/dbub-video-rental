const { get } = require('./db');
const SQL = require('sql-template-strings');

const PAGE_SIZE = 10;

const orderByClause = (tableName, { orderBy }) => {
  if (!orderBy || !Object.keys(orderBy).length) {
    return null;
  }

  const clause = SQL``;
  Object.keys(orderBy).forEach((field, i) => {
    const order = orderBy[field];
    if (!/\w+/.test(field)) {
      throw new Error(`Illegal field: "${field}"`);
    }
    if (i > 0) {
      clause.append(', ');
    }

    clause.append(`${field}`);
    if (order === 1) {
      clause.append(` ASC `);
    } else {
      clause.append(` DESC `);
    }
  });

  return clause.sql ? SQL` ORDER BY `.append(clause) : null;
};

const limitClause = (currentPage, pageSize) => {
  return SQL` LIMIT ${pageSize} OFFSET ${currentPage * pageSize}`
};

const whereClause = (tableName, { fields }) => {
  if (!fields || Object.keys(fields).length === 0) {
    return null;
  }

  const clause = SQL``;
  Object.keys(fields).forEach((field, i, all) => {
    const filter = fields[field];
    if (filter === '') {
      return;
    }
    if (!/\w+/.test(field)) {
      throw new Error(`Illegal field: "${field}"`);
    }

    if (i > 0) {
      clause.append(` AND `);
    }

    if (filter instanceof Array) {
      clause.append(`${tableName}.${field}`);
      clause.append(SQL` IN (${filter})`);
    } else if (field === 'id' || typeof filter === 'number') {
      clause.append(`${tableName}.${field}`);
      clause.append(SQL` = ${filter}`);
    } else if (filter) {
      clause.append(`${tableName}.${field}`);
      let term = `%${filter}%`;
      clause.append(SQL` LIKE ${term}`);
    } else {
      throw new Error(`UNKNOWN TERM: ${field} ${filter} ${i}`);
    }
  });

  return clause.sql ? SQL` WHERE `.append(clause) : null;
};

exports.find = async (tableName, sql, groupBy, query) => {
  const countSQL = SQL`SELECT count(*) as count
                       FROM `.append(`'${tableName}'`);

  const orderBy = orderByClause(tableName, query);
  const where = whereClause(tableName, query);
  const page = query.page || 0;

  if (where) {
    sql.append(where);
  }

  if (groupBy) {
    sql.append(groupBy)
  }

  if (orderBy) {
    sql.append(orderBy);
  }

  sql.append(limitClause(page, query.pageSize || PAGE_SIZE));

  const db = get();

  console.log(sql, '<<<<');
  console.log(countSQL, '<<<<<<');
  const [ items, total ] = await Promise.all([
    db.all(sql),
    db.get(countSQL)
  ]);

  const totalPages = Math.ceil(total.count / PAGE_SIZE);

  return { items, totalPages, currentPage: page };
};

