const { find } = require("./utils");

const { get } = require('./db');
const SQL = require('sql-template-strings');

const tableName = 'order';

exports.find = async (query) => {
  const baseSQL = SQL`
      SELECT 'order'.*,
             film.name   as filmName,
             client.name as clientName
      FROM 'order'
               LEFT JOIN film ON film.id = order.filmId
               LEFT JOIN client ON client.id = order.clientId
  `;


  return find(
    tableName,
    baseSQL,
    null,
    query
  );
};

exports.get = async (id) => {
  return await get().get(SQL`SELECT * FROM 'order' WHERE id = ${id}`);
};

exports.update = async (data) => {
  const { id, ...fields } = data;
  if (!id) {
    throw new Error('ID is missing');
  }

  // users are not allowed to change film/client for existing order
  delete data.filmId;
  delete data.clientId;

  const q = SQL`UPDATE 'order'
                SET `;
  Object.keys(fields).forEach((field, i) => {
    if (i > 0) {
      q.append(',');
    }
    if (!/\w+/.test(field)) {
      throw new Error(`Illegal field: "${field}"`);
    }
    q.append(field);
    q.append(SQL` = ${fields[field]} `);
  });

  q.append(SQL`WHERE id = ${id}`);

  await get().run(q);
  return true;
};

exports.insert = async (data) => {
  if (data.id) {
    throw new Error('not allowed to use IDs');
  }

  const db = get();
  data.createData = Math.ceil(Date.now() / 1000);

  const q = SQL`
    INSERT INTO 'order'(clientId, filmId, status, createDate, closeDate, price, bondTaken, bondReturned, discount) 
    VALUES( ${data.clientId}, ${data.filmId}, ${data.status}, ${data.createDate}, 0, ${data.price}, ${data.bondTaken}, 0, ${data.discount})
  `;
  const result = await db.run(q);
  return await db.get(SQL`SELECT * FROM 'order' WHERE id=${result.lastID}`);
};