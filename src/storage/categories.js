const { find } = require("./utils")

const { get } = require('./db');
const SQL = require('sql-template-strings');

const tableName = 'category';

exports.find = async (query) => {
  const baseQuery = SQL`
    SELECT category.*, count(film.id) as filmCnt 
    FROM category 
    LEFT JOIN film ON film.categoryId = category.id 
  `;

  return find(
    tableName,
    baseQuery,
    SQL` GROUP BY category.id `,
    query
  );
};

exports.get = async (id) => {
  return await get().get(SQL`SELECT * FROM category WHERE id = ${id}`);
};

exports.update = async (data) => {
  const { id, ...fields } = data;
  if (!id) {
    throw new Error('ID is missing');
  }

  const q = SQL`UPDATE category SET `;
  Object.keys(fields).forEach((field, i) => {
    if (i > 0) {
      q.append(',');
    }
    if (!/\w+/.test(field)) {
      throw new Error(`Illegal field: "${field}"`);
    }
    q.append(field);
    q.append(SQL` = ${fields[field]} `);
  });

  q.append(SQL`WHERE id = ${id}`);

  await get().run(q);
  return true;
};

exports.insert = async (data) => {
  if (data.id) {
    throw new Error('not allowed to use IDs');
  }

  const db = get();
  const q = SQL`INSERT INTO category(price, bond, name) VALUES( ${data.price}, ${data.bond}, ${data.name})`;
  const result = await db.run(q);
  return await db.get(SQL`SELECT * FROM category WHERE id=${result.lastID}`);
};