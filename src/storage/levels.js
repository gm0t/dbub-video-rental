const { find } = require("./utils");
const { get } = require('./db');
const SQL = require('sql-template-strings');

const tableName = 'loyaltyLevel';

exports.find = async (query) => {
  const baseSQL = SQL`
      SELECT loyaltyLevel.*, count(client.id) as clientCnt
      FROM loyaltyLevel
               LEFT JOIN client ON client.levelId = loyaltyLevel.id
  `;

  return find(
    tableName,
    baseSQL,
    SQL` GROUP BY loyaltyLevel.id `,
    query
  );
};

exports.get = async (id) => {
  return await get().get(SQL`SELECT * FROM loyaltyLevel WHERE id = ${id}`);
};

exports.update = async (data) => {
  const { id, ...fields } = data;
  if (!id) {
    throw new Error('ID is missing');
  }

  const q = SQL`UPDATE loyaltyLevel
                SET `;
  Object.keys(fields).forEach((field, i) => {
    if (i > 0) {
      q.append(',');
    }
    if (!/\w+/.test(field)) {
      throw new Error(`Illegal field: "${field}"`);
    }
    q.append(field);
    q.append(SQL` = ${fields[field]} `);
  });

  q.append(SQL`WHERE id = ${id}`);

  await get().run(q);
  return true;
};

exports.insert = async (data) => {
  if (data.id) {
    throw new Error('not allowed to use IDs');
  }

  const db = get();
  const q = SQL`INSERT INTO loyaltyLevel(discount, name, isActive) VALUES( ${data.discount}, ${data.name}, ${data.isActive})`;
  const result = await db.run(q);
  return await db.get(SQL`SELECT * FROM loyaltyLevel WHERE id=${result.lastID}`);
};