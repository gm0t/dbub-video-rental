--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS  category(
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   name VARCHAR(50) NOT NULL,
   price DECIMAL(6, 2) NOT NULL CHECK(price > 0),
   bond DECIMAL(6, 2) NOT NULL CHECK(bond > 0 )
);

CREATE TABLE IF NOT EXISTS  loyaltyLevel(
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   name VARCHAR(50) NOT NULL,
   isActive TINYINT NOT NULL CHECK(isActive IN (0, 1)),
   discount TINYINT NOT NULL CHECK(discount > 0 AND discount < 100)
);

CREATE TABLE IF NOT EXISTS  film(
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   name VARCHAR(50) NOT NULL,
   categoryId INTEGER NOT NULL,
   FOREIGN KEY (categoryId) REFERENCES category(id)
);

CREATE TABLE IF NOT EXISTS client (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   levelId INTEGER NOT NULL,
   name VARCHAR(50) NOT NULL,
   address VARCHAR(250) NOT NULL,
   phone VARCHAR(30) NOT NULL,
   FOREIGN KEY (levelId) REFERENCES loyaltyLevel(id)
);

CREATE TABLE IF NOT EXISTS `order` (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   clientId INTEGER,
   filmId INTEGER NOT NULL,
   status TINYINT NOT NULL,
   createDate INTEGER NOT NULL,
   closeDate INTEGER NOT NULL,
   price DECIMAL(6, 2) NOT NULL,
   bondTaken DECIMAL(6, 2) NOT NULL,
   bondReturned DECIMAL(6, 2) NOT NULL DEFAULT 0,
   discount DECIMAL(6, 2) NOT NULL DEFAULT 0,
   FOREIGN KEY(clientId) REFERENCES client(id),
   FOREIGN KEY(filmId) REFERENCES film(id)
);

INSERT INTO loyaltyLevel(name, isActive, discount) VALUES ('basic', 1, 10), ('Advanced', 1, 15), ('VIP', 1, 30), ('Promo', 0, 25);

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------

DROP TABLE `order`;
DROP TABLE `client`;
DROP TABLE `film`;
DROP TABLE `loyaltyLevel`;
DROP TABLE `category`;