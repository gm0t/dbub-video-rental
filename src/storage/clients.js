const { find } = require("./utils");

const { get } = require('./db');
const SQL = require('sql-template-strings');

const tableName = 'client';
exports.find = async (query) => {
  const baseQuery = SQL`SELECT *
                        FROM client `;

  return find(
    tableName,
    baseQuery,
    null,
    query
  );
};

exports.get = async (id) => {
  return await get().get(SQL`SELECT * FROM client WHERE id = ${id}`);
};

exports.update = async (data) => {
  const { id, ...fields } = data;
  if (!id) {
    throw new Error('ID is missing');
  }

  const q = SQL`UPDATE client
                SET `;
  Object.keys(fields).forEach((field, i) => {
    if (i > 0) {
      q.append(',');
    }
    if (!/\w+/.test(field)) {
      throw new Error(`Illegal field: "${field}"`);
    }
    q.append(field);
    q.append(SQL` = ${fields[field]} `);
  });

  q.append(SQL`WHERE id = ${id}`);

  await get().run(q);
  return true;
};

exports.insert = async (data) => {
  if (data.id) {
    throw new Error('not allowed to use IDs');
  }

  const db = get();
  const q = SQL`INSERT INTO client(levelId, name, address, phone) VALUES( ${data.levelId}, ${data.name}, ${data.address}, ${data.phone})`;
  const result = await db.run(q);
  return await db.get(SQL`SELECT * FROM client WHERE id=${result.lastID}`);
};