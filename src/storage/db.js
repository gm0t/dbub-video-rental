const sqlite = require('sqlite');
const path = require('path');

let db;
const init = async () => {
  db = await sqlite.open('./database.sqlite', { Promise });
  await db.run("PRAGMA foreign_keys = ON");
  await db.migrate({ migrationsPath: path.resolve(__dirname, 'migrations') });
  return true;
};

const get = () => {
  if (!db) {
    throw new Error('DB Connection was not initialized!');
  }

  return db;
};

process.on('exit', () => {
  if (db) {
    sqlite.close(db);
  }
});

module.exports = {
  init,
  get
};
