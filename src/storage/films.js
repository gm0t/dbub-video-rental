const { find } = require("./utils");

const { get } = require('./db');
const SQL = require('sql-template-strings');

const tableName = 'film';

exports.find = async (query) => {
  const baseSql = SQL`
  SELECT 
    film.*, 
    category.name as categoryName, 
    category.price, 
    category.bond,
    'order'.id as currentOrder
  FROM film 
  LEFT JOIN category ON category.id = film.categoryId
  LEFT JOIN 'order' ON 'order'.filmId = film.id AND 'order'.'status' = 1    
  `;

  return find(
    tableName,
    baseSql,
    SQL`GROUP BY film.id`,
    query
  );
};

exports.get = async (id) => {
  return await get().get(SQL`SELECT * FROM film WHERE id = ${id}`);
};

exports.update = async (data) => {
  const { id, ...fields } = data;
  if (!id) {
    throw new Error('ID is missing');
  }

  const q = SQL`UPDATE film SET `;
  Object.keys(fields).forEach((field, i) => {
    if (i > 0) {
      q.append(',');
    }
    if (!/\w+/.test(field)) {
      throw new Error(`Illegal field: "${field}"`);
    }
    q.append(field);
    q.append(SQL` = ${fields[field]} `);
  });

  q.append(SQL`WHERE id = ${id}`);

  await get().run(q);
  return true;
};

exports.insert = async (data) => {
  if (data.id) {
    throw new Error('not allowed to use IDs');
  }

  const db = get();
  const q = SQL`
  INSERT INTO film(name, description, categoryId, isActive) 
  VALUES( ${data.name}, ${data.description}, ${data.categoryId}, ${data.isActive})
  `;
  const result = await db.run(q);
  return await db.get(SQL`SELECT * FROM film WHERE id=${result.lastID}`);
};