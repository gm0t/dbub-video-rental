// in preload scripts, we have access to node.js and electron APIs
// the remote web app will not have access, so this is safe
const { ipcRenderer: ipc } = require('electron');

let requestId = 0;
let requests = {};

const standardApi = collection => ({
  find: find.bind(null, collection),
  get: get.bind(null, collection),
  update: update.bind(null, collection),
  create: create.bind(null, collection)
});

window.Bridge = {
  films: {
    ...standardApi('films'),
  },
  clients: {
    ...standardApi('clients'),
  },
  levels: {
    ...standardApi('levels'),
  },
  categories: {
    ...standardApi('categories'),
  },
  orders: {
    ...standardApi('orders'),
  },
  reports: {
    films: reports.bind(null, 'films'),
    money: reports.bind(null, 'money'),
  }
};

// we get this message from the main process
ipc.on('response', (event, { data, error, requestId }) => {
  if (requests[requestId]) {
    if (error) {
      requests[requestId].reject(error);
    } else {
      requests[requestId].resolve(data);
    }
    delete requests[requestId];
  }
});

function request(channel, params) {
  return new Promise((resolve, reject) => {
    requests[requestId] = { resolve, reject };
    ipc.send(channel, { ...params, requestId });
    requestId += 1;
  });
}

function find(collection, query) {
  return request('find', { collection, query });
}

function get(collection, itemId) {
  return request('get', { collection, itemId });
}

function update(collection, item) {
  return request('update', { collection, item });
}

function create(collection, item) {
  return request('create', { collection, item });
}

function reports(type, params) {
  return request('reports', { type, params });
}
