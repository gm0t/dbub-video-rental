import { Client, NewClient } from "./screens/clients/types";
import { Level, NewLevel } from "./objects/levels/types";
import { Film, NewFilm } from "./screens/films/types";
import { Category, NewCategory } from "./objects/categories/types";
import { Anonymous, NewOrder, Order } from "./screens/orders/types";

const { clients, films, levels, categories, orders, reports } = window.Bridge;

export const fetchClients = (query: Query) => {
    return clients.find(query);
};

export const fetchClient = (id: number | null): Promise<Client> => {
    if (id === null) {
        return Promise.resolve(Anonymous)
    }
    return clients.get(id);
};

export const updateClient = (client: Client) => {
    return clients.update(client);
};

export const createClient = (client: NewClient) => {
    return clients.create(client);
};

export const fetchLevels = (query: Query) => {
    return levels.find(query);
};

export const fetchLevel = (id: number) => {
    return levels.get(id);
};

export const updateLevel = (level: Level) => {
    return levels.update(level);
};

export const createLevel = (level: NewLevel) => {
    return levels.create(level);
};

export const fetchFilms = (query: Query) => {
    return films.find(query);
};

export const fetchFilm = (id: number) => {
    return films.get(id);
};

export const updateFilm = (film: Film) => {
    return films.update(film);
};

export const createFilm = (film: NewFilm) => {
    return films.create(film);
};

export const fetchOrders = (query: Query) => {
    return orders.find(query);
};

export const fetchOrder = (id: number) => {
    return orders.get(id);
};

export const updateOrder = (order: Order) => {
    return orders.update(order);
};

export const createOrder = (order: NewOrder) => {
    return orders.create(order);
};

export const fetchCategories = (query: Query) => {
    return categories.find(query);
};

export const fetchCategory = (id: number) => {
    return categories.get(id);
};

export const updateCategory = (category: Category) => {
    return categories.update(category);
};

export const createCategory = (category: NewCategory) => {
    return categories.create(category);
};

export const fetchFilmsReport = (from: number, to: number) => {
    return reports.films({ from, to })
};

export const fetchMoneyReport = (from: number, to: number) => {
    return reports.money({ from, to })
};