import { Client, NewClient } from "./screens/clients/types";
import { Level, NewLevel } from "./objects/levels/types";
import { LevelRow } from "./screens/levels/types";
import { CategoryRow } from "./screens/categories/types";
import { Category, NewCategory } from "./objects/categories/types";
import { Film, FilmRow, NewFilm } from "./screens/films/types";
import { NewOrder, Order, OrderRow } from "./screens/orders/types";

type StandardApi<T, NT, RT> = {
    find: (query: Query) => Promise<{
        items: RT[],
        currentPage: number,
        totalPages: number
    }>,
    get: (id: number) => Promise<T>,
    create: (newItem: NT) => Promise<T>,
    update: (item: { id: number } & Partial<T>) => Promise<T>,
};


type FilmsReportRow = {
    name: string,
    filmsCnt: number,
    ordersCnt: number,
    money: number
};

type MoneyReportRow = {
    day: string,
    cnt: number,
    money: number,
    fees: number,
    discounts: number
};

type ReportsApi = {
    films: (filter: { from: number, to: number }) => Promise<FilmsReportRow[]>,
    money: (filter: { from: number, to: number }) => Promise<MoneyReportRow[]>,
}

declare global {

    type Query = {
        fields?: Record<string, string | number | strin[] | number[]>,
        orderBy?: Record<string, 1 | -1>,
        page: number,
    }

    interface Window {
        Bridge: {
            films: StandardApi<Film, NewFilm, FilmRow>,
            orders: StandardApi<Order, NewOrder, OrderRow>,
            levels: StandardApi<Level, NewLevel, LevelRow>,
            clients: StandardApi<Client, NewClient, Client>,
            categories: StandardApi<Category, NewCategory, CategoryRow>,
            reports: ReportsApi
        }
    }
}