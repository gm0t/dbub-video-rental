import React, {
    useContext,
    useCallback,
    SyntheticEvent,
} from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Client, ClientData, NewClient, Updater } from './types';
import Spinner from "react-bootstrap/Spinner";
import { isLevelIdValid, isNameValid, useClient, useClients } from "./services";
import { useLevels } from "../../objects/levels/services";
import ClientContext, { ContextType } from "./context";

export const useEditClient = () => {
    const { setFormState, form: { isOpen, itemId } } = useContext<ContextType>(ClientContext);
    const open = useCallback(
        (itemId) => setFormState({ isOpen: true, itemId }),
        [setFormState]
    );
    const close = useCallback(() => {
        setFormState({ isOpen: false, itemId: null });
    }, [setFormState]);

    return {
        isOpen: !!isOpen,
        itemId,
        open,
        close
    };
};

type FormProps = { client: NewClient | Client, update: Updater<ClientData>, isSaving: boolean };

const ClientForm = ({ update, isSaving, client }: FormProps) => {
    const { levels } = useLevels();
    return (
        <Form>
            <Form.Group controlId="client.full-name">
                <Form.Label>Full name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="John Doe"
                    value={client.name}
                    disabled={isSaving}
                    isValid={isNameValid(client.name)}
                    isInvalid={!isNameValid(client.name)}
                    onChange={(e: SyntheticEvent<HTMLInputElement>) => update('name', e.currentTarget.value)}
                />
            </Form.Group>
            <Form.Group controlId="client.level">
                <Form.Label>Loyalty level</Form.Label>
                <Form.Control
                    as="select"
                    value={String(client.levelId)}
                    disabled={isSaving}
                    isValid={isLevelIdValid(client.levelId)}
                    isInvalid={!isLevelIdValid(client.levelId)}
                    onChange={(e: SyntheticEvent<HTMLInputElement>) => update('levelId', +e.currentTarget.value)}
                >
                    <option value={0} disabled>--choose--</option>
                    {levels.map(level => <option key={level.id} value={level.id}
                                                 disabled={!level.isActive}>{`${level.name} (${level.discount}%)`}</option>)}
                </Form.Control>
            </Form.Group>
        </Form>
    );
};

const EditClient = () => {
    const { isOpen, itemId, close } = useEditClient();
    const { refresh } = useClients();
    const { client, save, update, isSaving, isLoading, isValid, reset } = useClient(itemId);
    const cancel = () => {
        close();
        reset();
    };

    const onSaveClick = async () => {
        try {
            await save();
            refresh();
            cancel();
        } catch (e) {
            console.error(e);
        }
    };

    return (
        <Modal show={isOpen} onHide={cancel}>
            <Modal.Header closeButton>
                <Modal.Title>{itemId ? (client?.name || "...") : "New item"}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {client && !isLoading ? (
                    <ClientForm isSaving={isSaving} client={client} update={update} />
                ) : (
                    <div className="text-center">
                        <Spinner animation="grow" />
                    </div>
                )}
            </Modal.Body>
            <Modal.Footer>
                {isSaving ? <Spinner animation="grow" /> : null}
                <Button variant="secondary" onClick={cancel} disabled={isSaving}>
                    Cancel
                </Button>
                <Button variant="primary" onClick={onSaveClick} disabled={isSaving || !isValid}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    )
};

export default EditClient;