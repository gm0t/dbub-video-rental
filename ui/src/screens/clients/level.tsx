import React, { FunctionComponent } from 'react';
import Spinner from "react-bootstrap/Spinner";
import { useLevels } from "../../objects/levels/services";

const ClientLevel: FunctionComponent<{ levelId: number }> = ({ levelId }) => {
    const { levels, isLoading } = useLevels();
    if (isLoading) {
        return <Spinner animation="grow" />;
    }
    const level = levels.find(l => l.id === levelId);
    if (!level) {
        return <>`Unknown level: #${levelId}`</>;
    }

    return <>{`${level.name} (${level.discount}%)`}</>;
};

export default ClientLevel;