import React, { useEffect } from 'react';
import EditClient, { useEditClient } from "./edit-client";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useClients } from "./services";
import BsPagination from "react-bootstrap/Pagination";
import ClientLevel from "./level";
import { Client } from './types';
import Spinner from "react-bootstrap/Spinner";
import { ClientProvider } from "./context";
import { useSearch } from "../../components/search";

const Pagination = () => {
    const { load, currentPage, totalPages, isLoading } = useClients();
    const items = (new Array(totalPages)).fill(0).map((_, page) => {
        return (
            <BsPagination.Item
                key={page}
                active={page === currentPage}
                onClick={() => load(page)}
                disabled={isLoading}
            >
                {page + 1}
            </BsPagination.Item>
        );
    });

    return (
        <BsPagination size="sm">{items}</BsPagination>
    );
};

const ClientRow = ({ item }: { item: Client }) => {
    const { open } = useEditClient();
    return (
        <tr onClick={() => open(item.id)}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td><ClientLevel levelId={item.levelId} /></td>
        </tr>
    )
};

const ClientsList = () => {
    const { items, isLoading, setFilter } = useClients();
    const { query } = useSearch();

    useEffect(() => {
        setFilter(query);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query]);

    if (isLoading && !items.length) {
        return null;
    }

    return (
        <>
            <Table striped bordered hover className="editable sortable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Level</th>
                </tr>
                </thead>
                <tbody>
                {items.map(item => <ClientRow key={item.id} item={item} />)}
                </tbody>
            </Table>
            <Pagination />
        </>
    )
};

const ClientsTitle = () => {
    const { open } = useEditClient();
    const { isLoading } = useClients();

    return (
        <Row className="header">
            <Col xs={10}><h1>Clients {isLoading ? <Spinner animation="grow" /> : null}</h1></Col>
            <Col xs={2} className="text-right">
                <Button size="sm" onClick={() => open(null)}>Add</Button>
            </Col>
        </Row>
    );
};

const Clients = () => {
    return (
        <ClientProvider>
            <ClientsTitle />
            <ClientsList />
            <EditClient />
        </ClientProvider>
    );
};

export default Clients;