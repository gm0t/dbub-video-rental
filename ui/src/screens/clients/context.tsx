import { createScreenContext, ListState, FormState, Context } from "../../components/screen";
import { Client } from "./types";

const {
    Provider: ClientProvider,
    Context: ClientContext
} = createScreenContext<Client>();

export {
    ClientProvider
}

export type ListData = ListState<Client>;
export type FormData = FormState;
export type ContextType = Context<FormData, ListData>;

export default ClientContext;