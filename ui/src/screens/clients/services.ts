import { useContext, useEffect, useState } from "react";
import { Client, ClientData, NewClient, Updater } from "./types";
import ClientContext, { ContextType } from "./context";
import { createClient, fetchClient, fetchClients, updateClient } from "../../api";

export const isNameValid = (name: string): boolean => name.length > 5;
export const isLevelIdValid = (levelId: number): boolean => levelId > 0;

const saveClient = (data: Client | NewClient) => {
    if (data.id === null) {
        return createClient(data);
    } else {
        return updateClient(data);
    }
};

export const useClient = (clientId: number | null) => {
    const [client, setClient] = useState<Client | NewClient | null>(null);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isSaving, setIsSaving] = useState<boolean>(false);
    const [version, setVersion] = useState<number>(0);

    const fetch = async (clientId: number | null) => {
        if (!clientId) {
            // TODO: fix default data
            setClient({ id: null, name: '', levelId: 0 });
            setIsLoading(false);
            return;
        }

        setIsLoading(true);
        const data = await fetchClient(clientId);
        setClient(data);
        setIsLoading(false);
    };

    useEffect(() => {
        fetch(clientId);
    }, [clientId, version]);

    const save = async () => {
        if (!client) {
            throw new Error("Client is null");
        }
        setIsSaving(true);
        await saveClient(client);
        setIsSaving(false);
    };

    const update: Updater<ClientData> = (field, value) => {
        if (client) {
            setClient(({ ...client, [field]: value }))
        }
    };

    const isValid = (): boolean => !!(client && isNameValid(client?.name) && isLevelIdValid(client.levelId));
    const reset = () => setVersion(version + 1);

    return {
        client,
        update,
        isLoading,
        isSaving,
        isValid: isValid(),
        reset,
        save
    }
};

export const useClients = () => {
    const { list, setListState } = useContext<ContextType>(ClientContext);

    const load = async (page: number) => {
        setListState({ ...list, isLoading: true });
        const result = await fetchClients({ page, fields: list.filter });
        setListState({ ...result, filter: list.filter, isLoading: false });
    };

    const refresh = async () => {
        load(list.currentPage);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    };

    const setFilter = (name: string) => {
        setListState({ ...list, filter: { name } });
    };

    // this line is also responsible for initial fetching
    useEffect(() => {
        refresh();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [list.filter]);

    return {
        ...list,
        setFilter,
        load,
        refresh
    }
};