export type ClientData = {
    name: string,
    levelId: number
}

export type Client = ClientData & {
    id: number,
}

export type NewClient = ClientData & {
    id: null,
}

export type Updater<T> = (k: keyof T, v: string | number) => void;