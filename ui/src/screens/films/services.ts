import { useContext, useEffect, useState } from "react";
import Context, { ContextType } from "./context";
import { createFilm, fetchFilm, fetchFilms, updateFilm } from "../../api";
import { Film, FilmData, NewFilm, Updater } from "./types";
import { isValid } from "./validators";
import { Filter } from "../../components/screen";

export const useFilms = () => {
    const { list, setListState } = useContext<ContextType>(Context);
    const load = async (page: number, filter?: Filter) => {
        if (!filter) {
            filter = list.filter;
        }

        setListState({ ...list, isLoading: true });
        const result = await fetchFilms({ page, fields: filter, orderBy: { currentOrder: 1 } });
        setListState({ ...result, filter, isLoading: false });
    };

    const refresh = async () => {
        load(list.currentPage);
    };

    return {
        ...list,
        refresh,
        load
    }
};

const saveFilm = (data: Film | NewFilm) => {
    if (data.id === null) {
        return createFilm(data);
    } else {
        return updateFilm(data);
    }
};

export const useFilm = (itemId: number | null) => {
    const [data, setData] = useState<Film | NewFilm | null>(null);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isSaving, setIsSaving] = useState<boolean>(false);
    const [version, setVersion] = useState<number>(0);

    const fetch = async (itemId: number | null) => {
        if (!itemId) {
            setData({ id: null, name: '', isActive: 1, description: '', categoryId: 0 });
            setIsLoading(false);
            return;
        }

        setIsLoading(true);
        const data = await fetchFilm(itemId);
        setData(data);
        setIsLoading(false);
    };

    useEffect(() => {
        fetch(itemId);
    }, [itemId, version]);

    const save = async () => {
        if (!data) {
            throw new Error("Item is null");
        }
        setIsSaving(true);
        await saveFilm(data);
        setIsSaving(false);
    };

    const update: Updater<FilmData> = (field, value) => {
        if (data) {
            setData(({ ...data, [field]: value }))
        }
    };

    const reset = () => setVersion(version + 1);

    return {
        data,
        update,
        isLoading,
        isSaving,
        isValid: data && isValid(data),
        reset,
        save
    }
};
