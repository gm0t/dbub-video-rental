export type FilmData = {
    name: string,
    description: string,
    categoryId: number,
    isActive: 0 | 1
}

export type Film = FilmData & {
    id: number,
}

export type NewFilm = FilmData & {
    id: null,
}

export type Films = Film[];

export type Updater<T> = (k: keyof T, v: string | number) => void;

export type FilmRow = Film & {
    currentOrder: null | number,
    categoryName: string
};