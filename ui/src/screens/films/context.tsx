import { createScreenContext, ListState, FormState, Context } from "../../components/screen";
import { FilmRow } from "./types";

const {
    Provider: FilmProvider,
    Context: FilmContext
} = createScreenContext<FilmRow>();

export {
    FilmProvider
}

export type ListData = ListState<FilmRow>;
export type FormData = FormState;
export type ContextType = Context<FormData, ListData>;

export default FilmContext;