import { Film, NewFilm } from "./types";

export const isNameValid = (name: string): boolean => !!name && name.length >= 3 && name.length <= 50;
export const isDescriptionValid = (description: string): boolean => !!description && description.length >= 10 && description.length <= 200;
export const isCategoryValid = (categoryId: number): boolean => categoryId > 0;

export const isValid = (item: NewFilm | Film): boolean => (
    isNameValid(item.name)
    && isDescriptionValid(item.description)
    && isCategoryValid(item.categoryId)
);

export default isValid;