import React, {
    useContext,
    useCallback,
    SyntheticEvent,
} from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Spinner from "react-bootstrap/Spinner";
import { useFilm, useFilms } from "./services";
import Context, { ContextType } from "./context";
import { isDescriptionValid, isNameValid } from "./validators";
import { Film, FilmData, NewFilm, Updater } from "./types";
import { useCategories } from "../../objects/categories/services";

export const useEditForm = () => {
    const { setFormState, form: { isOpen, itemId } } = useContext<ContextType>(Context);
    const open = useCallback(
        (itemId) => setFormState({ isOpen: 'edit', itemId }),
        [setFormState]
    );
    const close = useCallback(() => {
        setFormState({ isOpen: false, itemId: null });
    }, [setFormState]);

    return {
        isOpen: isOpen === 'edit',
        itemId,
        open,
        close
    };
};

type FilmFormProps = { item: NewFilm | Film, update: Updater<FilmData>, isSaving: boolean };

const FilmForm = ({ update, isSaving, item }: FilmFormProps) => {
    const { items: categories } = useCategories();
    return (
        <Form>
            <Form.Group controlId="film.name">
                <Form.Label>Title</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Title"
                    value={item.name}
                    disabled={isSaving}
                    isValid={isNameValid(item.name)}
                    isInvalid={!isNameValid(item.name)}
                    onChange={(e: SyntheticEvent<HTMLInputElement>) => update('name', e.currentTarget.value)}
                />
            </Form.Group>
            <Form.Group controlId="film.description">
                <Form.Label>Description</Form.Label>
                <Form.Control
                    as={"textarea"}
                    placeholder="Description"
                    value={item.description}
                    disabled={isSaving}
                    isValid={isDescriptionValid(item.description)}
                    isInvalid={!isDescriptionValid(item.description)}
                    onChange={(e: SyntheticEvent<HTMLInputElement>) => update('description', e.currentTarget.value)}
                />
            </Form.Group>
            <Form.Group controlId="film.category">
                <Form.Label>Category</Form.Label>
                <Form.Control
                    as="select"
                    placeholder="%"
                    value={String(item.categoryId)}
                    disabled={isSaving}
                    onChange={(e: SyntheticEvent<HTMLInputElement>) => update('categoryId', +e.currentTarget.value)}
                >
                    <option value={0} disabled>-- choose --</option>
                    {categories.map(item => (
                        <option key={item.id} value={item.id}>
                            {item.name} ({item.price}/{item.bond})
                        </option>
                    ))}
                </Form.Control>
            </Form.Group>
            <Form.Group controlId="film.isActive">
                <Form.Label>Status</Form.Label>
                <Form.Control
                    as="select"
                    placeholder="%"
                    value={String(item.isActive)}
                    disabled={isSaving}
                    onChange={(e: SyntheticEvent<HTMLInputElement>) => update('isActive', +e.currentTarget.value)}
                >
                    <option value={0}>Disabled</option>
                    <option value={1}>Active</option>
                </Form.Control>
            </Form.Group>
        </Form>
    );
}

const Edit = () => {
    const { isOpen, itemId, close } = useEditForm();
    const { refresh } = useFilms();
    const { data, save, update, isSaving, isLoading, isValid, reset } = useFilm(itemId);
    const cancel = () => {
        close();
        reset();
    };

    const onSaveClick = async () => {
        try {
            await save();
            refresh();
            cancel();
        } catch (e) {
            console.error(e);
        }
    };

    return (
        <Modal show={isOpen} onHide={cancel}>
            <Modal.Header closeButton>
                <Modal.Title>{itemId ? (data?.name || "...") : "New item"}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {data && !isLoading ? (
                    <FilmForm isSaving={isSaving} item={data} update={update} />
                ) : (
                    <div className="text-center">
                        <Spinner animation="grow" />
                    </div>
                )}
            </Modal.Body>
            <Modal.Footer>
                {isSaving ? <Spinner animation="grow" /> : null}
                <Button variant="secondary" onClick={cancel} disabled={isSaving}>
                    Cancel
                </Button>
                <Button variant="primary" onClick={onSaveClick} disabled={isSaving || !isValid}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    )
};

export default Edit;