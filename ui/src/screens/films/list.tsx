import React, { useEffect } from 'react';
import Edit, { useEditForm } from "./edit";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useFilms } from "./services";
import BsPagination from "react-bootstrap/Pagination";
import Spinner from "react-bootstrap/Spinner";
import { FilmProvider } from "./context";
import { FilmRow } from "./types";
import { useSearch } from "../../components/search";
import Card from "react-bootstrap/Card";
import './films.css';
import CreateModal, { useCreateForm } from "../orders/create";
import { OrderProvider } from "../orders/context";
import Badge from "react-bootstrap/Badge";
import ViewModal, { useViewForm } from "../orders/view";

type Item = FilmRow;

const Pagination = () => {
    const { load, currentPage, totalPages, isLoading } = useFilms();
    if (totalPages < 2) {
        return null;
    }
    const items = (new Array(totalPages)).fill(0).map((_, page) => {
        return (
            <BsPagination.Item
                key={page}
                active={page === currentPage}
                onClick={() => load(page)}
                disabled={isLoading}
            >
                {page + 1}
            </BsPagination.Item>
        );
    });

    return (
        <BsPagination size="sm">{items}</BsPagination>
    );
};

function calcBg(item: FilmRow) {
    if (!item.isActive) {
        return 'secondary';
    }
    if (item.currentOrder) {
        return 'light';
    }

    return 'primary';
}

const ItemCmp = ({ item }: { item: Item }) => {
    const { open: edit } = useEditForm();
    const { open: createOrder } = useCreateForm();
    const { open: viewOrder } = useViewForm();
    const { id, isActive, categoryName, name, currentOrder, description } = item;
    return (
        <Card className="film-card" bg={calcBg(item)}>
            <Card.Header>{name} <Badge variant="light">{categoryName}</Badge></Card.Header>
            <Card.Body>
                <Card.Text>
                    {description}
                </Card.Text>
            </Card.Body>
            <Card.Footer>
                {isActive && !currentOrder ? <Button
                    size="sm"
                    variant="success"
                    onClick={() => createOrder(id)}
                >
                    Rent
                </Button> : null}
                {currentOrder ? <Button
                    size="sm"
                    variant="success"
                    onClick={() => viewOrder(currentOrder)}
                >
                    View order
                </Button> : null}
                <Button size="sm" variant="secondary" onClick={() => edit(id)}>Edit</Button>
            </Card.Footer>
        </Card>
    )
};

const List = () => {
    const { items, currentPage, isLoading, load } = useFilms();
    const { query } = useSearch();

    useEffect(() => {
        load(currentPage, { name: query, isActive: 1 });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query, currentPage]);

    if (isLoading && !items.length) {
        return null;
    }

    return (
        <div className="films-list">
            {items.map(item => <ItemCmp key={item.id} item={item} />)}
        </div>
    )
};

const Title = () => {
    const { open } = useEditForm();
    const { isLoading } = useFilms();

    return (
        <Row className="header">
            <Col xs={10}><h1>Films {isLoading ? <Spinner animation="grow" /> : null}</h1></Col>
            <Col xs={2} className="text-right">
                <Button size="sm" onClick={() => open(null)}>Add</Button>
            </Col>
        </Row>
    );
};

const Films = () => {
    return (
        <OrderProvider>
            <FilmProvider>
                <Title />
                <List />
                <Pagination />
                <Edit />
                <CreateModal />
                <ViewModal />
            </FilmProvider>
        </OrderProvider>
    );
};

export default Films;