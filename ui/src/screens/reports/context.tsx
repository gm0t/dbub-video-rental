import React, { createContext, FunctionComponent, useContext, useEffect, useState } from "react";

export type Filter = {
    from: number,
    to: number
};

export type State = {
    filter: Filter,
}

export type Context = {
    filter: Filter,
    setFilter: (filter: Filter) => void,
}

const defaultFilter = () => {
    const now = new Date();
    return ({
        to: Math.floor(Date.now() / 1000),
        from: Math.ceil((new Date()).setDate(now.getDate() - 7) / 1000)
    });
};

const defaultState: State = {
    filter: defaultFilter()
};

const noop = () => null;

const Context = createContext<Context>({
    ...defaultState,
    setFilter: noop,
});

const Provider: FunctionComponent = ({ children }) => {
    const [filter, setFilter] = useState<Filter>(defaultState.filter);
    const { Provider } = Context;

    useEffect(() => {
        setFilter(defaultFilter());
    }, []);

    const context: Context = {
        filter,
        setFilter,
    };

    return <Provider value={context}>{children}</Provider>
};

const useReports = () => {
    return useContext(Context);
};

export {
    Provider,
    useReports
}


