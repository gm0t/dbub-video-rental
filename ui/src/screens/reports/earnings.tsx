import React, { useState, useEffect } from 'react';
import { useReports } from "./context";
import { fetchMoneyReport } from "../../api";
import { MoneyReportRow } from "../../bridge";
import Table from "react-bootstrap/Table";
import Spinner from "react-bootstrap/Spinner";

const useFilmReports = () => {
    const { filter: { from, to } } = useReports();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [report, setReport] = useState<MoneyReportRow[]>([]);

    const fetchReport = async () => {
        setIsLoading(true);
        const report = await fetchMoneyReport(from, to);
        setReport(report);
        setIsLoading(false);
    };

    useEffect(() => {
        fetchReport()
    }, [from, to]);

    return { report, isLoading }
};

const EarningsReport = () => {
    const { report, isLoading } = useFilmReports();
    if (isLoading) {
        return <Spinner animation="grow" />
    }

    return (
        <Table striped bordered hover className="editable sortable">
            <thead>
            <tr>
                <th>Date</th>
                <th className="text-right"># of orders</th>
                <th className="text-right">Discounts</th>
                <th className="text-right">Money</th>
                <th className="text-right">Fees</th>
                <th className="text-right">Total</th>
            </tr>
            </thead>
            <tbody>
            {report.map((item, i) => (
                <tr key={i}>
                    <td>{report[i].day}</td>
                    <td className="text-right">{report[i].cnt}</td>
                    <td className="text-right">{report[i].discounts}</td>
                    <td className="text-right">{report[i].money}$</td>
                    <td className="text-right">{report[i].fees}$</td>
                    <td className="text-right">{report[i].money + report[i].fees}$</td>
                </tr>
            ))}
            </tbody>
        </Table>
    )
};

export default EarningsReport;