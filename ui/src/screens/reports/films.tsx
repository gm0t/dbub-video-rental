import React, { useState, useEffect } from 'react';
import { useReports } from "./context";
import { fetchFilmsReport } from "../../api";
import { FilmsReportRow } from "../../bridge";
import Table from "react-bootstrap/Table";
import Spinner from "react-bootstrap/Spinner";

const useFilmReports = () => {
    const { filter: { from, to } } = useReports();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [report, setReport] = useState<FilmsReportRow[]>([]);

    const fetchReport = async () => {
        setIsLoading(true);
        const report = await fetchFilmsReport(from, to);
        setReport(report);
        setIsLoading(false);
    };

    useEffect(() => {
        console.log('!fetching!');
        fetchReport()
    }, [from, to]);

    return { report, isLoading }
};

const FilmsReport = () => {
    const { report, isLoading } = useFilmReports();
    if (isLoading) {
        return <Spinner animation="grow" />
    }

    return (
        <Table striped bordered hover className="editable sortable">
            <thead>
            <tr>
                <th>Category</th>
                <th className="text-right"># of films</th>
                <th className="text-right"># of orders</th>
                <th className="text-right">$ earned</th>
            </tr>
            </thead>
            <tbody>
            {report.map((item, i) => (
                <tr key={i}>
                    <td>{report[i].name}</td>
                    <td className="text-right">{report[i].filmsCnt}</td>
                    <td className="text-right">{report[i].ordersCnt}</td>
                    <td className="text-right">{(report[i].money || 0).toFixed(2)}$</td>
                </tr>
            ))}
            </tbody>
        </Table>
    )
};

export default FilmsReport;