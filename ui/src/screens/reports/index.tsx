import React from 'react';

import FilmsReport from './films';
import EarningsReport from './earnings';
import Tab from "react-bootstrap/Tab";
import Card from "react-bootstrap/Card";
import Nav from "react-bootstrap/Nav";
import { Provider } from "./context";

import "./reports.css"

const ReportsScreen = () => {
    return (
        <Provider>
            <Tab.Container id="reports" defaultActiveKey="earnings">
                <Card className="reports-wrapper">
                    <Card.Header>
                        <Nav variant="pills">
                            <Nav.Item><Nav.Link eventKey="earnings">Earnings</Nav.Link></Nav.Item>
                            <Nav.Item><Nav.Link eventKey="films">Films</Nav.Link></Nav.Item>
                        </Nav>
                    </Card.Header>
                    <Card.Body>
                        <Tab.Content>
                            <Tab.Pane eventKey="films">
                                <FilmsReport />
                            </Tab.Pane>
                            <Tab.Pane eventKey="earnings">
                                <EarningsReport />
                            </Tab.Pane>
                        </Tab.Content>
                    </Card.Body>
                </Card>
            </Tab.Container>
        </Provider>
    )
};

export default ReportsScreen;