import { createScreenContext, ListState, FormState, Context } from "../../components/screen";
import { OrderRow } from "./types";

const {
    Provider: OrderProvider,
    Context: OrderContext
} = createScreenContext<OrderRow>();

export {
    OrderProvider
}

export type ListData = ListState<OrderRow>;
export type FormData = FormState;
export type ContextType = Context<FormData, ListData>;

export default OrderContext;