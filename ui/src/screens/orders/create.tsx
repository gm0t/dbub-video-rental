import React, {
    useContext,
    useCallback, useEffect,
} from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Spinner from "react-bootstrap/Spinner";
import { useCreateOrder } from "./services";
import Context, { ContextType } from "./context";
import ClientSelector from "../../components/client-selector";
import Badge from "react-bootstrap/Badge";
import ListGroup from "react-bootstrap/ListGroup";
import { Client } from "../clients/types";
import { Order } from "./types";
import Alert from "react-bootstrap/Alert";
import { Category } from "../../objects/categories/types";
import { Level } from "../../objects/levels/types";
import { useFilms } from "../films/services";

export const useCreateForm = () => {
    const { setFormState, form: { isOpen, itemId } } = useContext<ContextType>(Context);
    const open = useCallback(
        (itemId) => setFormState({ isOpen: 'rent', itemId }),
        [setFormState]
    );
    const close = useCallback(() => {
        setFormState({ isOpen: false, itemId: null });
    }, [setFormState]);

    return {
        isOpen: isOpen === 'rent',
        itemId,
        open,
        close
    };
};

type RentFormProps = { filmId: number, cancel: () => void };


const CreateFormResult = ({ order }: { order: Order }) => {
    const { refresh } = useFilms();
    useEffect(() => {
        refresh();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return (
        <Alert variant="success">
            Success! Order #{order.id}
        </Alert>
    );
};

const CreateFormBody = (props: { client: Client | null; onClientChange: (c: Client) => void; category: Category | null; loyaltyLevel: Level | null; }) => {
    const { client, onClientChange, category, loyaltyLevel } = props;
    return (
        <Form>
            <Form.Group>
                <Form.Label>Loyalty card:</Form.Label>
                <ClientSelector value={client?.id || 0} onChange={onClientChange} />
            </Form.Group>
            <Form.Group>
                <ListGroup variant="flush">
                    <ListGroup.Item><strong>Category:</strong> &nbsp; <Badge
                        variant="primary">{category?.name}</Badge></ListGroup.Item>
                    <ListGroup.Item><strong>Price</strong>: &nbsp; {category?.price}</ListGroup.Item>
                    <ListGroup.Item><strong>Bond</strong>: &nbsp; {category?.bond}</ListGroup.Item>
                    {loyaltyLevel ?
                        <ListGroup.Item><strong>Discount</strong>: &nbsp; {loyaltyLevel?.discount}%</ListGroup.Item> : null}
                </ListGroup>
            </Form.Group>
        </Form>
    );
};


const CreateForm = ({ filmId, cancel }: RentFormProps) => {
    const {
        isSaving, isRefreshingCategory, isRefreshingLoyaltyLevel,
        category, loyaltyLevel, film, client, savedOrder,
        setClientId,
        save,
        finalPrice
    } = useCreateOrder(filmId);
    const areControlsLocked = isSaving || isRefreshingCategory || isRefreshingLoyaltyLevel;

    const onSaveClick = async () => {
        try {
            await save();
        } catch (e) {
            console.error(e);
        }
    };

    const onClientChange = (client: Client) => {
        setClientId(client.id)
    };

    return (
        <>
            <Modal.Header closeButton>
                <Modal.Title>
                    Renting "{film ? film.name : "..."}"
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {savedOrder
                    ? <CreateFormResult order={savedOrder} />
                    : <CreateFormBody onClientChange={onClientChange} client={client} loyaltyLevel={loyaltyLevel}
                                      category={category} />}
            </Modal.Body>
            {
                savedOrder ? (
                    <Modal.Footer>
                        <Button variant="primary" onClick={cancel}>
                            Close
                        </Button>
                    </Modal.Footer>
                ) : (
                    <Modal.Footer>
                        {areControlsLocked ? <Spinner animation="grow" /> : null}
                        <Button variant="secondary" onClick={cancel} disabled={areControlsLocked}>
                            Cancel
                        </Button>
                        <Button variant="primary" onClick={onSaveClick} disabled={areControlsLocked}>
                            Rent for {finalPrice}$
                        </Button>
                    </Modal.Footer>
                )
            }
        </>
    );
};

const CreateModal = () => {
    const { isOpen, itemId, close } = useCreateForm();
    const { refresh } = useFilms();
    const cancel = () => {
        refresh();
        close();
    };

    return (
        <Modal show={isOpen} onHide={cancel}>
            {itemId ? <CreateForm filmId={itemId} cancel={cancel} /> : null}
        </Modal>
    )
};

export default CreateModal;