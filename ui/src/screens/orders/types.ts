import { Film } from "../films/types";
import { Client } from "../clients/types";

export const Anonymous: Client = {
    id: -1,
    name: 'Anonymous',
    levelId: -1
};

export enum ORDER_STATUS {
    ACTIVE = 1,
    CLOSED = 2,
}

export enum ORDER_DEADLINES {
    FIRST = 5,
    FINAL = 10,
}

export type OrderData = {
    filmId: number,
    clientId: number | null,
    status: number,
    createDate: number,
    closeDate: number,
    price: number,
    bondTaken: number,
    bondReturned: number,
    discount: number,
}

export type Order = OrderData & {
    id: number,
}

export type NewOrder = OrderData & {
    id: null,
}

export type Orders = Order[];

export type Updater<T> = (k: keyof T, v: string | number | Film | Client) => void;

export type OrderRow = Order & {
    film: Film,
    client: Client,
};