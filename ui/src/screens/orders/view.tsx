import React, {
    useContext,
    useCallback,
} from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Spinner from "react-bootstrap/Spinner";
import { useViewOrder } from "./services";
import Context, { ContextType } from "./context";
import { Client } from "../clients/types";
import { Order, ORDER_DEADLINES, ORDER_STATUS } from "./types";
import { Level } from "../../objects/levels/types";
import { useFilms } from "../films/services";
import { Film } from "../films/types";

import './view.css';

export const useViewForm = () => {
    const { setFormState, form: { isOpen, itemId } } = useContext<ContextType>(Context);
    const open = useCallback(
        (itemId) => setFormState({ isOpen: 'view', itemId }),
        [setFormState]
    );
    const close = useCallback(() => {
        setFormState({ isOpen: false, itemId: null });
    }, [setFormState]);

    return {
        isOpen: isOpen === 'view',
        itemId,
        open,
        close
    };
};

type RentFormProps = { orderId: number, cancel: () => void };

const OrderDetails = (props: { client: Client | null; loyaltyLevel: Level; film: Film, order: Order, activeDays: number }) => {
    const { client, order, loyaltyLevel, film, activeDays } = props;
    let daysClassName = 'text-success';
    let fee = '';
    if (activeDays > ORDER_DEADLINES.FINAL) {
        daysClassName = 'text-danger';
        fee = '(fee 100%)';
    } else if (activeDays > ORDER_DEADLINES.FIRST) {
        daysClassName = 'text-warning';
        fee = '(fee 50%)';
    }

    console.log(client);
    return (
        <div>
            <h3>Information</h3>
            <dl>
                <dt>Film</dt>
                <dd>{film.name}</dd>
                <dt>Price</dt>
                <dd>{order.price}</dd>
                <dt>Bond taken</dt>
                <dd>{order.bondTaken}</dd>
                <dt>Rent started</dt>
                <dd>{formatDate(order.createDate)}</dd>
                <dt>Rent closed</dt>
                <dd>{formatDate(order.closeDate)}</dd>
                <dt className={daysClassName}>Days</dt>
                <dd className={daysClassName}>{activeDays} {fee}</dd>
                {order.status === ORDER_STATUS.CLOSED ? (
                    <>
                        <dt>Bond returned</dt>
                        <dd>{order.bondReturned}</dd>
                    </>
                ) : null}
            </dl>
            {client && loyaltyLevel ? (
                <>
                    <h3>Client</h3>
                    <dl>
                        <dt>Name</dt>
                        <dd>{client.name}</dd>
                        <dt>Loyalty level</dt>
                        <dd>{loyaltyLevel.name}</dd>
                    </dl>
                </>
            ) : null}
        </div>
    );
};

const z = (v: number) => v < 10 ? `0${v}` : v;

const formatDate = (unix: number) => {
    if (!unix) {
        return '--/--/----';
    }
    const date = new Date(unix * 1000);
    return `${z(date.getDate())}/${z(date.getMonth())}/${date.getFullYear()}`;
}

const ViewForm = ({ orderId, cancel }: RentFormProps) => {
    const {
        isSaving,
        isLoading,
        loyaltyLevel,
        film,
        client,
        order,
        bondToReturn,
        activeDays,
        closeOrder,
    } = useViewOrder(orderId);
    const { refresh: refreshFilms } = useFilms();

    if (isLoading) {
        return <Modal.Body><Spinner animation="grow" /></Modal.Body>
    }

    const onOrderClose = async () => {
        try {
            await closeOrder()
            refreshFilms();
            cancel();
        } catch (e) {
            console.error('Failed to close order:', e);
        }
    };

    return (
        <>
            <Modal.Header closeButton>
                <Modal.Title>
                    Order #{orderId}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <OrderDetails
                    order={order!}
                    client={client}
                    film={film!}
                    loyaltyLevel={loyaltyLevel!}
                    activeDays={activeDays}
                />
            </Modal.Body>
            <Modal.Footer>
                {isSaving ? <Spinner animation="grow" /> : null}
                <Button variant="secondary" onClick={cancel} disabled={isSaving}>
                    Cancel
                </Button>
                {order?.status === ORDER_STATUS.ACTIVE ?
                    <Button variant="primary" onClick={onOrderClose} disabled={isSaving}>
                        Close order (bond: {bondToReturn} $)
                    </Button> : null
                }
            </Modal.Footer>
        </>
    );
};

const ViewModal = () => {
    const { isOpen, itemId, close } = useViewForm();
    const { refresh } = useFilms();
    const cancel = () => {
        refresh();
        close();
    };

    return (
        <Modal show={isOpen} onHide={cancel} className="view-order">
            {itemId ? <ViewForm orderId={itemId} cancel={cancel} /> : null}
        </Modal>
    )
};

export default ViewModal;