import { useEffect, useState } from "react";
import {
    createOrder,
    fetchCategory,
    fetchClient,
    fetchFilm,
    fetchLevel, fetchOrder,
    updateOrder
} from "../../api";
import { Order, NewOrder, ORDER_STATUS, ORDER_DEADLINES } from "./types";
import { Film } from "../films/types";
import { Client } from "../clients/types";
import { Category } from "../../objects/categories/types";
import { Level } from "../../objects/levels/types";

const saveOrder = (data: Order | NewOrder) => {
    if (data.id === null) {
        return createOrder(data);
    } else {
        return updateOrder(data);
    }
};

function daysDiff(start: number, end: number) {
    return Math.floor((end - start) / 86400)
}

export const useViewOrder = (orderId: number) => {
    const [isSaving, setIsSaving] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(true);

    const [order, setOrder] = useState<Order | null>(null);
    const [film, setFilm] = useState<Film | null>(null);
    const [category, setCategory] = useState<Category | null>(null);
    const [client, setClient] = useState<Client | null>(null);
    const [loyaltyLevel, setLoyaltyLevel] = useState<Level | null>(null);

    const updateFilm = async (filmId: number) => {
        const film = await fetchFilm(filmId);
        const category = await fetchCategory(film.categoryId);
        setFilm(film);
        setCategory(category);
    };

    const updateClient = async (clientId: number | null) => {
        const client = await fetchClient(clientId)
        const level = client.levelId ? await fetchLevel(client.levelId) : null;
        setClient(client);
        setLoyaltyLevel(level);
    };

    const fetch = async () => {
        setIsLoading(true);
        const order = await fetchOrder(orderId);
        setOrder(order);
        await Promise.all([updateClient(order.clientId), updateFilm(order.filmId)]);
        setIsLoading(false);
    };

    const activeDays = () => {
        if (!order) {
            return 0
        }
        return daysDiff(order.createDate, order.status === ORDER_STATUS.ACTIVE ? Date.now() / 1000 : order.closeDate);
    };

    const bondToReturn = () => {
        if (!order || order.status === ORDER_STATUS.CLOSED) {
            return '-/-'
        }

        const days = activeDays();
        if (days < ORDER_DEADLINES.FIRST) {
            return order.bondTaken.toFixed(2);
        } else if (days < ORDER_DEADLINES.FINAL) {
            return (order.bondTaken * 0.5).toFixed(2)
        } else {
            return "0.00"
        }
    };

    const closeOrder = async () => {
        setIsSaving(true);
        const result = await updateOrder({
            ...order!,
            status: ORDER_STATUS.CLOSED,
            closeDate: Math.floor(Date.now() / 1000),
            bondReturned: Math.ceil(+bondToReturn())
        });
        setIsSaving(false);
        return result;
    };

    useEffect(() => {
        fetch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [orderId]);

    return {
        bondToReturn: bondToReturn(),
        activeDays: activeDays(),
        isSaving,
        film,
        category,
        client,
        order,
        loyaltyLevel,
        isLoading,
        closeOrder
    }
};

export const useCreateOrder = (filmId: number) => {
    const [savedOrder, setOrder] = useState<Order | null>(null);
    const [isSaving, setIsSaving] = useState<boolean>(false);
    const [version, setVersion] = useState<number>(0);

    const [isRefreshingCategory, setIsRefreshingCategory] = useState<boolean>(false);
    const [category, setCategory] = useState<Category | null>(null);
    const [film, setFilm] = useState<Film | null>(null);
    const [isRefreshingLoyaltyLevel, setIsRefreshingLoyalty] = useState<boolean>(false);
    const [client, setClient] = useState<Client | null>(null);
    const [loyaltyLevel, setLoyaltyLevel] = useState<Level | null>(null);

    const [clientId, setClientId] = useState<number | null>(null)

    const updateCategory = async () => {
        setIsRefreshingCategory(true);
        const film = await fetchFilm(filmId);
        const category = await fetchCategory(film.categoryId);
        setFilm(film);
        setCategory(category);
        setIsRefreshingCategory(false);
    };

    const updateLoyaltyLevel = async () => {
        setIsRefreshingLoyalty(true);
        const client = await fetchClient(clientId)
        const level = client.levelId ? await fetchLevel(client.levelId) : null;
        setClient(client);
        setLoyaltyLevel(level);
        setIsRefreshingLoyalty(false);
    };

    const finalPrice = () => {
        if (!category) {
            return '-/-'
        }

        return loyaltyLevel ? (category.price * (100 - loyaltyLevel.discount) / 100).toFixed(2) : category.price.toFixed(2);
    };

    useEffect(() => {
        updateCategory();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [filmId]);

    useEffect(() => {
        updateLoyaltyLevel();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [clientId]);

    const save = async () => {
        setIsSaving(true);
        if (!category) {
            throw new Error("Invalid State: category is missing");
        }

        const order = await saveOrder({
            id: null,
            filmId: filmId,
            clientId: clientId,
            status: ORDER_STATUS.ACTIVE,
            price: +finalPrice(),
            bondTaken: category.bond,
            closeDate: 0,
            createDate: Math.ceil(Date.now() / 1000),
            bondReturned: 0,
            discount: loyaltyLevel?.discount || 0
        });

        setIsSaving(false);
        setOrder(order);
    };

    const reset = () => setVersion(version + 1);

    return {
        finalPrice: finalPrice(),
        savedOrder,
        isSaving,
        isRefreshingLoyaltyLevel,
        isRefreshingCategory,
        film,
        category,
        client,
        loyaltyLevel,
        setClientId,
        reset,
        save
    }
};