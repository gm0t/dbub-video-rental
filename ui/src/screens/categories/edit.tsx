import React, {
    useContext,
    useCallback,
    SyntheticEvent,
} from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Spinner from "react-bootstrap/Spinner";
import { useCategory, useCategories } from "./services";
import Context, { ContextType } from "./context";
import { isPriceValid, isBondValid, isNameValid } from "../../objects/categories/validators";
import { Category, CategoryData, NewCategory, Updater } from "../../objects/categories/types";

export const useEditForm = () => {
    const { setFormState, form: { isOpen, itemId } } = useContext<ContextType>(Context);
    const open = useCallback(
        (itemId) => setFormState({ isOpen: true, itemId }),
        [setFormState]
    );
    const close = useCallback(() => {
        setFormState({ isOpen: false, itemId: null });
    }, [setFormState]);

    return {
        isOpen,
        itemId,
        open,
        close
    };
};

type CategoryFormProps = { item: NewCategory | Category, update: Updater<CategoryData>, isSaving: boolean };

const CategoryForm = ({ update, isSaving, item }: CategoryFormProps) => (
    <Form>
        <Form.Group controlId="category.name">
            <Form.Label>Title</Form.Label>
            <Form.Control
                type="text"
                placeholder="Title"
                value={item.name}
                disabled={isSaving}
                isValid={isNameValid(item.name)}
                isInvalid={!isNameValid(item.name)}
                onChange={(e: SyntheticEvent<HTMLInputElement>) => update('name', e.currentTarget.value)}
            />
        </Form.Group>
        <Form.Group controlId="category.price">
            <Form.Label>Price</Form.Label>
            <Form.Control
                type="number"
                placeholder="Price"
                value={String(item.price)}
                disabled={isSaving}
                isValid={isPriceValid(item.price)}
                isInvalid={!isPriceValid(item.price)}
                onChange={(e: SyntheticEvent<HTMLInputElement>) => update('price', e.currentTarget.value)}
            />
        </Form.Group>
        <Form.Group controlId="category.bond">
            <Form.Label>Bond</Form.Label>
            <Form.Control
                type="number"
                placeholder="Bond"
                value={String(item.bond)}
                isValid={isBondValid(item.price)}
                isInvalid={!isBondValid(item.price)}
                disabled={isSaving}
                onChange={(e: SyntheticEvent<HTMLInputElement>) => update('bond', +e.currentTarget.value)}
            />
        </Form.Group>
    </Form>
);

const Edit = () => {
    const { isOpen, itemId, close } = useEditForm();
    const { refresh } = useCategories();
    const { data, save, update, isSaving, isLoading, isValid, reset } = useCategory(itemId);
    const cancel = () => {
        close();
        reset();
    };

    const onSaveClick = async () => {
        try {
            await save();
            refresh();
            cancel();
        } catch (e) {
            console.error(e);
        }
    };

    return (
        <Modal show={!!isOpen} onHide={cancel}>
            <Modal.Header closeButton>
                <Modal.Title>{itemId ? (data?.name || "...") : "New category"}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {data && !isLoading ? (
                    <CategoryForm isSaving={isSaving} item={data} update={update} />
                ) : (
                    <div className="text-center">
                        <Spinner animation="grow" />
                    </div>
                )}
            </Modal.Body>
            <Modal.Footer>
                {isSaving ? <Spinner animation="grow" /> : null}
                <Button variant="secondary" onClick={cancel} disabled={isSaving}>
                    Cancel
                </Button>
                <Button variant="primary" onClick={onSaveClick} disabled={isSaving || !isValid}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    )
};

export default Edit;