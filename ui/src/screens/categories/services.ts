import { useContext, useEffect, useState } from "react";
import Context, { ContextType } from "./context";
import { createCategory, fetchCategory, fetchCategories, updateCategory } from "../../api";
import { Category, CategoryData, NewCategory, Updater } from "../../objects/categories/types";
import { useCategories as useObjects } from "../../objects/categories/services";
import { isValid } from "../../objects/categories/validators";
import { Filter } from "../../components/screen";

export const useCategories = () => {
    const { list, setListState } = useContext<ContextType>(Context);
    const load = async (page: number, filter?: Filter) => {
        if (!filter) {
            filter = list.filter;
        }

        setListState({ ...list, isLoading: true });
        const result = await fetchCategories({ page, fields: filter });
        setListState({ ...result, filter, isLoading: false });
    };

    const refresh = async () => {
        load(list.currentPage);
    };

    return {
        ...list,
        refresh,
        load
    }
};

const saveCategory = (data: Category | NewCategory) => {
    if (data.id === null) {
        return createCategory(data);
    } else {
        return updateCategory(data);
    }
};

export const useCategory = (itemId: number | null) => {
    const [data, setData] = useState<Category | NewCategory | null>(null);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isSaving, setIsSaving] = useState<boolean>(false);
    const [version, setVersion] = useState<number>(0);
    const { refresh } = useObjects()

    const fetch = async (itemId: number | null) => {
        if (!itemId) {
            setData({ id: null, name: '', price: 50, bond: 100 });
            setIsLoading(false);
            return;
        }

        setIsLoading(true);
        const data = await fetchCategory(itemId);
        setData(data);
        setIsLoading(false);
    };

    useEffect(() => {
        fetch(itemId);
    }, [itemId, version]);

    const save = async () => {
        if (!data) {
            throw new Error("Item is null");
        }
        setIsSaving(true);
        await saveCategory(data);
        refresh();
        setIsSaving(false);
    };

    const update: Updater<CategoryData> = (field, value) => {
        if (data) {
            setData(({ ...data, [field]: value }))
        }
    };

    const reset = () => setVersion(version + 1);

    return {
        data,
        update,
        isLoading,
        isSaving,
        isValid: data && isValid(data),
        reset,
        save
    }
};