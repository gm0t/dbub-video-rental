import React, { useEffect } from 'react';
import Edit, { useEditForm } from "./edit";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useCategories } from "./services";
import BsPagination from "react-bootstrap/Pagination";
import Spinner from "react-bootstrap/Spinner";
import { CategoryProvider } from "./context";
import { CategoryRow } from "./types";
import { useSearch } from "../../components/search";

type Item = CategoryRow;

const Pagination = () => {
    const { load, currentPage, totalPages, isLoading } = useCategories();
    const items = (new Array(totalPages)).fill(0).map((_, page) => {
        return (
            <BsPagination.Item
                key={page}
                active={page === currentPage}
                onClick={() => load(page)}
                disabled={isLoading}
            >
                {page + 1}
            </BsPagination.Item>
        );
    });

    return (
        <BsPagination size="sm">{items}</BsPagination>
    );
};

const RowCmp = ({ item }: { item: Item }) => {
    const { open } = useEditForm();
    return (
        <tr onClick={() => open(item.id)}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.price} $</td>
            <td>{item.bond} $</td>
            <td>{item.filmCnt}</td>
        </tr>
    )
};

const List = () => {
    const { items, currentPage, isLoading, load } = useCategories();
    const { query } = useSearch();

    useEffect(() => {
        load(currentPage, { name: query });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query]);

    if (isLoading && !items.length) {
        return null;
    }

    return (
        <>
            <Table striped bordered hover className="editable sortable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Bond</th>
                    <th># of films</th>
                </tr>
                </thead>
                <tbody>
                {items.map(item => <RowCmp key={item.id} item={item} />)}
                </tbody>
            </Table>
            <Pagination />
        </>
    )
};

const Title = () => {
    const { open } = useEditForm();
    const { isLoading } = useCategories();

    return (
        <Row className="header">
            <Col xs={10}><h1>Film categories {isLoading ? <Spinner animation="grow" /> : null}</h1></Col>
            <Col xs={2} className="text-right">
                <Button size="sm" onClick={() => open(null)}>Add</Button>
            </Col>
        </Row>
    );
};

const Categories = () => {
    return (
        <CategoryProvider>
            <Title />
            <List />
            <Edit />
        </CategoryProvider>
    );
};

export default Categories;