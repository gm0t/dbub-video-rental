import { createScreenContext, ListState, FormState, Context } from "../../components/screen";
import { CategoryRow } from "./types";

const {
    Provider: CategoryProvider,
    Context: CategoryContext
} = createScreenContext<CategoryRow>();

export {
    CategoryProvider
}

export type ListData = ListState<CategoryRow>;
export type FormData = FormState;
export type ContextType = Context<FormData, ListData>;

export default CategoryContext;