import { Category } from "../../objects/categories/types";

export type CategoryRow = Category & {
    filmCnt: number
};