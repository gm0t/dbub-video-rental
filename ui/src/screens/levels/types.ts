import { Level } from "../../objects/levels/types";

export type LevelRow = Level & {
    clientCnt: number
};