import { useContext, useEffect, useState } from "react";
import Context, { ContextType } from "./context";
import { createLevel, fetchLevel, fetchLevels, updateLevel } from "../../api";
import { Level, LevelData, NewLevel, Updater } from "../../objects/levels/types";
import { useLevels as useObjects } from "../../objects/levels/services";
import { isValid } from "../../objects/levels/validators";
import { Filter } from "../../components/screen";

export const useLevels = () => {
    const { list, setListState } = useContext<ContextType>(Context);
    const load = async (page: number, filter?: Filter) => {
        if (!filter) {
            filter = list.filter;
        }

        setListState({ ...list, isLoading: true });
        const result = await fetchLevels({ page, fields: filter });
        setListState({ ...result, filter, isLoading: false });
    };

    const refresh = async () => {
        load(list.currentPage);
    };

    return {
        ...list,
        refresh,
        load
    }
};

const saveLevel = (data: Level | NewLevel) => {
    if (data.id === null) {
        return createLevel(data);
    } else {
        return updateLevel(data);
    }
};

export const useLevel = (itemId: number | null) => {
    const [data, setData] = useState<Level | NewLevel | null>(null);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isSaving, setIsSaving] = useState<boolean>(false);
    const [version, setVersion] = useState<number>(0);
    const { refresh } = useObjects()

    const fetch = async (itemId: number | null) => {
        if (!itemId) {
            setData({ id: null, name: '', isActive: 1, discount: 5 });
            setIsLoading(false);
            return;
        }

        setIsLoading(true);
        const data = await fetchLevel(itemId);
        setData(data);
        setIsLoading(false);
    };

    useEffect(() => {
        fetch(itemId);
    }, [itemId, version]);

    const save = async () => {
        if (!data) {
            throw new Error("Item is null");
        }
        setIsSaving(true);
        await saveLevel(data);
        refresh();
        setIsSaving(false);
    };

    const update: Updater<LevelData> = (field, value) => {
        if (data) {
            setData(({ ...data, [field]: value }))
        }
    };

    const reset = () => setVersion(version + 1);

    return {
        data,
        update,
        isLoading,
        isSaving,
        isValid: data && isValid(data),
        reset,
        save
    }
};