import { createScreenContext, ListState, FormState, Context } from "../../components/screen";
import { LevelRow } from "./types";

const {
    Provider: LevelProvider,
    Context: LevelContext
} = createScreenContext<LevelRow>();

export {
    LevelProvider
}

export type ListData = ListState<LevelRow>;
export type FormData = FormState;
export type ContextType = Context<FormData, ListData>;

export default LevelContext;