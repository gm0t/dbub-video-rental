import React, {
    useContext,
    useCallback,
    SyntheticEvent,
} from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Spinner from "react-bootstrap/Spinner";
import { useLevel, useLevels } from "./services";
import Context, { ContextType } from "./context";
import { isDiscountValid, isNameValid } from "../../objects/levels/validators";
import { Level, LevelData, NewLevel, Updater } from "../../objects/levels/types";

export const useEditForm = () => {
    const { setFormState, form: { isOpen, itemId } } = useContext<ContextType>(Context);
    const open = useCallback(
        (itemId) => setFormState({ isOpen: true, itemId }),
        [setFormState]
    );
    const close = useCallback(() => {
        setFormState({ isOpen: false, itemId: null });
    }, [setFormState]);

    return {
        isOpen: !!isOpen,
        itemId,
        open,
        close
    };
};

type LevelFormProps = { level: NewLevel | Level, update: Updater<LevelData>, isSaving: boolean };

const LevelForm = ({ update, isSaving, level }: LevelFormProps) => (
    <Form>
        <Form.Group controlId="level.name">
            <Form.Label>Title</Form.Label>
            <Form.Control
                type="text"
                placeholder="Title"
                value={level.name}
                disabled={isSaving}
                isValid={isNameValid(level.name)}
                isInvalid={!isNameValid(level.name)}
                onChange={(e: SyntheticEvent<HTMLInputElement>) => update('name', e.currentTarget.value)}
            />
        </Form.Group>
        <Form.Group controlId="level.discount">
            <Form.Label>Discount</Form.Label>
            <Form.Control
                type="number"
                placeholder="%"
                value={String(level.discount)}
                disabled={isSaving}
                isValid={isDiscountValid(level.discount)}
                isInvalid={!isDiscountValid(level.discount)}
                onChange={(e: SyntheticEvent<HTMLInputElement>) => update('discount', e.currentTarget.value)}
            />
        </Form.Group>
        <Form.Group controlId="level.isActive">
            <Form.Label>Status</Form.Label>
            <Form.Control
                as="select"
                placeholder="%"
                value={String(level.isActive)}
                disabled={isSaving}
                onChange={(e: SyntheticEvent<HTMLInputElement>) => update('isActive', +e.currentTarget.value)}
            >
                <option value={0}>Disabled</option>
                <option value={1}>Active</option>
            </Form.Control>
        </Form.Group>
    </Form>
);

const Edit = () => {
    const { isOpen, itemId, close } = useEditForm();
    const { refresh } = useLevels();
    const { data, save, update, isSaving, isLoading, isValid, reset } = useLevel(itemId);
    const cancel = () => {
        close();
        reset();
    };

    const onSaveClick = async () => {
        try {
            await save();
            refresh();
            cancel();
        } catch (e) {
            console.error(e);
        }
    };

    return (
        <Modal show={isOpen} onHide={cancel}>
            <Modal.Header closeButton>
                <Modal.Title>{itemId ? (data?.name || "...") : "New item"}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {data && !isLoading ? (
                    <LevelForm isSaving={isSaving} level={data} update={update} />
                ) : (
                    <div className="text-center">
                        <Spinner animation="grow" />
                    </div>
                )}
            </Modal.Body>
            <Modal.Footer>
                {isSaving ? <Spinner animation="grow" /> : null}
                <Button variant="secondary" onClick={cancel} disabled={isSaving}>
                    Cancel
                </Button>
                <Button variant="primary" onClick={onSaveClick} disabled={isSaving || !isValid}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    )
};

export default Edit;