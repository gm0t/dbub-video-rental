import React, { useEffect } from 'react';
import Edit, { useEditForm } from "./edit";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useLevels } from "./services";
import BsPagination from "react-bootstrap/Pagination";
import Spinner from "react-bootstrap/Spinner";
import { LevelProvider } from "./context";
import { LevelRow } from "./types";
import { useSearch } from "../../components/search";

type Item = LevelRow;

const Pagination = () => {
    const { load, currentPage, totalPages, isLoading } = useLevels();
    const items = (new Array(totalPages)).fill(0).map((_, page) => {
        return (
            <BsPagination.Item
                key={page}
                active={page === currentPage}
                onClick={() => load(page)}
                disabled={isLoading}
            >
                {page + 1}
            </BsPagination.Item>
        );
    });

    return (
        <BsPagination size="sm">{items}</BsPagination>
    );
};

const RowCmp = ({ item }: { item: Item }) => {
    const { open } = useEditForm();
    return (
        <tr onClick={() => open(item.id)}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.discount} %</td>
            <td>{item.clientCnt}</td>
            <td>{item.isActive ? 'Active' : 'Disabled'}</td>
        </tr>
    )
};

const List = () => {
    const { items, currentPage, isLoading, load } = useLevels();
    const { query } = useSearch();

    useEffect(() => {
        load(currentPage, { name: query });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query]);

    if (isLoading && !items.length) {
        return null;
    }

    return (
        <>
            <Table striped bordered hover className="editable sortable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Discount</th>
                    <th># of Clients</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                {items.map(item => <RowCmp key={item.id} item={item} />)}
                </tbody>
            </Table>
            <Pagination />
        </>
    )
};

const Title = () => {
    const { open } = useEditForm();
    const { isLoading } = useLevels();

    return (
        <Row className="header">
            <Col xs={10}><h1>Loyalty levels {isLoading ? <Spinner animation="grow" /> : null}</h1></Col>
            <Col xs={2} className="text-right">
                <Button size="sm" onClick={() => open(null)}>Add</Button>
            </Col>
        </Row>
    );
};

const Levels = () => {
    return (
        <LevelProvider>
            <Title />
            <List />
            <Edit />
        </LevelProvider>
    );
};

export default Levels;