import React, { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Form from 'react-bootstrap/Form';
import Films from "./screens/films";
import Clients from "./screens/clients";
import Reports from "./screens/reports";
import ObjectsProvider from "./objects";
import Levels from "./screens/levels";
import Categories from "./screens/categories";
import { SearchInput, SearchProvider, useSearch } from "./components/search";

const Content = ({ section }: { section: string }) => {
    switch (section) {
        case 'films':
            return <Films />;
        case 'clients':
            return <Clients />;
        case 'reports':
            return <Reports />;
        case 'levels':
            return <Levels />;
        case 'categories':
            return <Categories />;
        default:
            return null
    }
};

const Navigation = ({ onNavigate }: { onNavigate: (s: string) => void }) => {
    const { setSearch } = useSearch();
    const setSection = (section: string) => {
        setSearch('');
        onNavigate(section);
    };

    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">Video Rental</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="#films" onClick={() => setSection('films')}>Films</Nav.Link>
                    <Nav.Link href="#reports" onClick={() => setSection('reports')}>Reports</Nav.Link>
                    <Nav.Link href="#clients" onClick={() => setSection('clients')}>Clients</Nav.Link>
                    <Nav.Link href="#levels" onClick={() => setSection('levels')}>Loyalty levels</Nav.Link>
                    <Nav.Link href="#categories" onClick={() => setSection('categories')}>Categories</Nav.Link>
                </Nav>
                <Form inline>
                    <SearchInput className="mr-sm-2" />
                </Form>
            </Navbar.Collapse>
        </Navbar>
    );
};

function App() {
    const [section, setSection] = useState('reports');

    return (
        <ObjectsProvider>
            <SearchProvider>
                <Container className="app-wrapper">
                    <Navigation onNavigate={setSection} />
                    <Content section={section} />
                </Container>
            </SearchProvider>
        </ObjectsProvider>
    );
}

export default App;
