import React, { createContext, FunctionComponent, useState } from "react";

export type Filter = Record<string, string | number | string[] | number[]>;

export type ListState<T> = {
    items: T[],
    filter: Filter,
    currentPage: number,
    totalPages: number,
    isLoading: boolean,
}

export type FormState = {
    isOpen: boolean | string,
    itemId: null | number,
}

export type State<F, L> = {
    list: L,
    form: F
}

export type Context<F, L> = {
    form: F,
    list: L,
    setListState: (s: L) => void,
    setFormState: (s: F) => void
}

export function createScreenContext<T>() {
    type F = FormState;
    type L = ListState<T>;
    const defaultState: State<F, L> = {
        list: { items: [], currentPage: 0, isLoading: false, totalPages: 0, filter: {}, },
        form: { isOpen: false, itemId: null }
    };

    const noop = () => null;

    const Context = createContext<Context<any, any>>({
        ...defaultState,
        setListState: noop,
        setFormState: noop
    });

    const Provider: FunctionComponent = ({ children }) => {
        const [list, setListState] = useState<L>(defaultState.list);
        const [form, setFormState] = useState<F>(defaultState.form);

        const { Provider } = Context;

        const context: Context<F, L> = {
            list,
            setListState,
            form,
            setFormState
        };

        return <Provider value={context}>{children}</Provider>
    };

    return {
        Provider,
        Context
    }
}
