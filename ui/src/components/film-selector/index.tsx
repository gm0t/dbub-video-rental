import createSelector from "../selector";
import { Film } from "../../screens/films/types";
import { fetchFilms, fetchFilm } from "../../api";


const optionsFetcher = async (filter: string): Promise<Film[]> => {
    const result = await fetchFilms({ fields: { name: filter }, page: 0 });
    return result.items;
};

const FilmSelector = createSelector<Film>(optionsFetcher, fetchFilm, film => film.name);
export default FilmSelector;