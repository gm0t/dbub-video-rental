import createSelector from "../selector";
import { fetchClients, fetchClient } from "../../api";
import { Client } from "../../screens/clients/types";

const optionsFetcher = async (filter: string): Promise<Client[]> => {
    const result = await fetchClients({ fields: { name: filter }, page: 0 });
    return result.items;
};

const ClientSelector = createSelector<Client>(optionsFetcher, fetchClient, client => client.name);
export default ClientSelector;