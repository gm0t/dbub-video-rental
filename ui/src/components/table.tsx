import React from 'react';
import BsTable from "react-bootstrap/Table";

const Table = () => {
    return (
        <BsTable striped bordered hover className="editable sortable">
            <thead>
            <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
            </tr>
            </tbody>
        </BsTable>
    )
};

export default Table;
