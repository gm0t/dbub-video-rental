import { useContext } from "react";
import Context from './context';

export const useSearch = () => {
    const { query, setSearch } = useContext(Context);
    return { query, setSearch };
};

