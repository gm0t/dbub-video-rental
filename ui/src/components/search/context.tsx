import React, { createContext, FunctionComponent, useState } from 'react';

type State = {
    query: string,
}

const Context = createContext<State & { setSearch: (s: string) => void }>({
    query: '',
    setSearch: () => null
});

export const SearchProvider: FunctionComponent = ({ children }) => {
    const { Provider } = Context;
    const [query, setSearch] = useState<string>('');
    const context = {
        query,
        setSearch
    };

    return <Provider value={context}>{children}</Provider>
};

export default Context;