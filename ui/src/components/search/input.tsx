import React, { SyntheticEvent } from "react";
import FormControl from "react-bootstrap/FormControl";
import { useSearch } from "./hooks";

type Props = { className: string };

const SearchInput = ({ className }: Props) => {
    const { query, setSearch } = useSearch();
    const onChange = (e: SyntheticEvent<HTMLInputElement>) => setSearch(e.currentTarget.value);

    return <FormControl type="text" value={query} onChange={onChange} placeholder="Search" className={className} />;
};

export default SearchInput;