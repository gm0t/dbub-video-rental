import React, { ComponentType, useEffect, useState } from "react";
import Select from 'react-select/async';

type Resolver<T> = (id: number) => Promise<T>;
type Fetcher<T> = (filter: string) => Promise<T[]>;
type Formatter<T> = (option: T) => string;

function useResolvedValue<T>(itemId: number | null, resolver: Resolver<T>): [T | null, boolean] {
    const [item, setItem] = useState<T | null>(null);
    const [isReady, setIsReady] = useState<boolean>(false);

    const fetchItem = async (id: number) => {
        setIsReady(false);
        const item = await resolver(id);
        setItem(item);
        setIsReady(true);
    };

    useEffect(() => {
        if (!itemId) {
            setItem(null);
            setIsReady(true);
            return;
        } else {
            fetchItem(itemId);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [itemId]);

    return [item, isReady];
}

type Props<T> = {
    value: number | null,
    onChange: (item: any) => void,
}

function createSelector<T extends Record<string, any>>(
    optionsFetcher: Fetcher<T>,
    valueResolver: Resolver<T>,
    optionFormatter: Formatter<T>
): ComponentType<Props<T>> {
    const SelectorComponent = (props: Props<T>) => {
        const [resolvedValue, isReady] = useResolvedValue<T>(props.value, valueResolver);

        return (
            <Select
                value={resolvedValue}
                cacheOptions
                loadOptions={optionsFetcher}
                formatOptionLabel={optionFormatter}
                isLoading={!isReady}
                onChange={props.onChange}
                isClearable
                defaultOptions
            />
        )
    };
    return SelectorComponent;
}

export default createSelector;