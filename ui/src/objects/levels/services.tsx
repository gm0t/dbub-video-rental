import { useContext } from "react";
import LevelsContext from "./context";
import { fetchLevels } from "../../api";

export const useLevels = () => {
    const { levels, isLoading, isInitialized, setState } = useContext(LevelsContext);

    const loadLevels = async () => {
        if (!isLoading) {
            setState({ levels, isLoading: true, isInitialized: false });
            const { items } = await fetchLevels({ page: 0 });
            setState({ levels: items, isLoading: false, isInitialized: true });
        }
    };

    const refresh = () => loadLevels();

    if (!isInitialized) {
        loadLevels();
    }

    return {
        isLoading,
        refresh,
        levels
    }
}