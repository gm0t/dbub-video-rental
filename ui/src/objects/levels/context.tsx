import React, { createContext, FunctionComponent, useState } from 'react';
import { Levels } from "./types";

type State = {
    levels: Levels,
    isLoading: boolean,
    isInitialized: boolean,
}

const defaultState = { levels: [], isLoading: false, isInitialized: false };
const LevelsContext = createContext<State & { setState: (s: State) => void }>({ ...defaultState, setState: () => null });

export const LevelsProvider: FunctionComponent = ({ children }) => {
    const { Provider } = LevelsContext;
    const [state, setState] = useState<State>(defaultState)
    const context = {
        ...state,
        setState
    };

    return <Provider value={context}>{children}</Provider>
}

export default LevelsContext;