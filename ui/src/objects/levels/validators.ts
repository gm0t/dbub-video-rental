import { Level, NewLevel } from "./types";

export const isDiscountValid = (discount: number): boolean => discount > 0 && discount < 100
export const isNameValid = (name: string): boolean => !!name && name.length >= 3;

export const isValid = (level: NewLevel | Level): boolean => isNameValid(level.name) && isDiscountValid(level.discount);

export default isValid;