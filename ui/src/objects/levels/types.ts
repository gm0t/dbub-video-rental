export type LevelData = {
    name: string,
    discount: number,
    isActive: 0 | 1
}

export type Level = LevelData & {
    id: number,
}

export type NewLevel = LevelData & {
    id: null,
}

export type Levels = Level[];

export type Updater<T> = (k: keyof T, v: string | number) => void;