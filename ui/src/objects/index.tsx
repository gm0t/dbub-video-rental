import { FunctionComponent } from "react";
import * as React from "react";
import { LevelsProvider } from "./levels/context";
import { CategoriesProvider } from "./categories/context";

const ObjectsProvider: FunctionComponent = ({ children }) => (
    <LevelsProvider>
        <CategoriesProvider>
            {children}
        </CategoriesProvider>
    </LevelsProvider>
);

export default ObjectsProvider;