import { Category, NewCategory } from "./types";

export const isBondValid = (bond: number): boolean => bond > 0;
export const isPriceValid = (price: number): boolean => price > 0;
export const isNameValid = (name: string): boolean => !!name && name.length >= 3 && name.length <= 50;

export const isValid = (item: NewCategory | Category): boolean => (
    isNameValid(item.name) && isBondValid(item.bond) && isPriceValid(item.price)
);

export default isValid;