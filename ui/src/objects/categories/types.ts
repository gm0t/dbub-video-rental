export type CategoryData = {
    name: string,
    price: number,
    bond: number,
}

export type Category = CategoryData & {
    id: number,
}

export type NewCategory = CategoryData & {
    id: null,
}

export type Categories = Category[];

export type Updater<T> = (k: keyof T, v: string | number) => void;