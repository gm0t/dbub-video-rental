import { useContext } from "react";
import CategoriesContext from "./context";
import { fetchCategories } from "../../api";

export const useCategories = () => {
    const { items, isLoading, isInitialized, setState } = useContext(CategoriesContext);

    const load = async () => {
        if (!isLoading) {
            setState({ items, isLoading: true, isInitialized: false });
            const { items: nitems } = await fetchCategories({ page: 0 });
            setState({ items: nitems, isLoading: false, isInitialized: true });
        }
    };

    const refresh = () => load();

    if (!isInitialized) {
        load();
    }

    return {
        isLoading,
        refresh,
        items: items
    }
}