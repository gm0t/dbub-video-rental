import React, { createContext, FunctionComponent, useState } from 'react';
import { Categories } from "./types";

type State = {
    items: Categories,
    isLoading: boolean,
    isInitialized: boolean,
}

const defaultState = { items: [], isLoading: false, isInitialized: false };
const CategoriesContext = createContext<State & { setState: (s: State) => void }>({ ...defaultState, setState: () => null });

export const CategoriesProvider: FunctionComponent = ({ children }) => {
    const { Provider } = CategoriesContext;
    const [state, setState] = useState<State>(defaultState)
    const context = {
        ...state,
        setState
    };

    return <Provider value={context}>{children}</Provider>
}

export default CategoriesContext;